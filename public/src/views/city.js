import {Vector} from "../core/maths.js";
import MapView from "../mapview.js";
import base from "../themes/base.js";
import dawn from "../themes/dawn.js";
import day from "../themes/day.js";
import dusk from "../themes/dusk.js";
import night from "../themes/night.js";

export default class City extends MapView {
	constructor(harp, canvas) {
		super(harp, {
			canvas,
			range: new Vector(16, 20),
			source: {
				type: 'vector'
			},
			theme: {
				base,
				// dawn,
				// day,
				// dusk,
				// night
			},
			type: 'planar'
		});
	}
}
