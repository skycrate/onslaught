import {Vector} from "../core/maths.js";
import MapView from "../mapview.js";
import base from "../themes/base.js";
import space from "../themes/space.js";

export default class Globe extends MapView {
	atmosphere;
	constructor(harp, canvas) {
		super(harp, {
			canvas,
			range: new Vector(1, 14),
			source: {
				type: 'satellite'
			},
			theme: {
				base,
				space
			},
			type: 'sphere'
		});
		const { mapAnchors, camera, projection, update } = this._view;
		this.atmosphere = new harp.MapViewAtmosphere(mapAnchors, camera, projection, update.bind(this._view));
		this.atmosphere.lightMode = harp.AtmosphereLightMode.LightDynamic;
	}
}
