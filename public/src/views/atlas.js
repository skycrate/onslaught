import Mapstract from "../mapstract.js";
import harp from "../../harp.js";
import Globe from "./globe.js";
import City from "./city.js";

export default class Atlas extends Mapstract {
	globe;
	city;
	constructor(dom) {
		this.globe = new Globe(harp, dom.globe, false);
		this.city = new City(harp, dom.city, false);
	}
	// Switch between map views...
	toggle() {
		// 
	}
}
