const PI = Math.PI;
const RAD  = PI / 180;
const e = RAD * 23.4397; // obliquity of the Earth

const dayMs = 1000 * 60 * 60 * 24, J1970 = 2440588, J2000 = 2451545, J0 = 0.0009;

function toJulian(date) {
	return date.valueOf() / dayMs - 0.5 + J1970;
}
function fromJulian(j) {
	return new Date((j + 0.5 - J1970) * dayMs);
}
function toDays(date) {
	return toJulian(date) - J2000;
}
function julianCycle(d, lw) {
	return Math.round(d - J0 - lw / (2 * PI));
}

function approxTransit(Ht, lw, n) {
	return J0 + (Ht + lw) / (2 * PI) + n;
}
function solarTransitJ(ds, M, L) {
	return J2000 + ds + 0.0053 * Math.sin(M) - 0.0069 * Math.sin(2 * L);
}

function hourAngle(h, phi, d) {
	return Math.acos((Math.sin(h) - Math.sin(phi) * Math.sin(d)) / (Math.cos(phi) * Math.cos(d)));
}
function observerAngle(height) {
	return -2.076 * Math.sqrt(height) / 60;
}

// returns set time for the given sun altitude
function getSetJ(h, lw, phi, dec, n, M, L) {
	return solarTransitJ(approxTransit(hourAngle(h, phi, dec), lw, n), M, L);
}

function rightAscension(l, b) {
	return Math.atan2(Math.sin(l) * Math.cos(e) - Math.tan(b) * Math.sin(e), Math.cos(l));
}
function declination(l, b) {
	return Math.asin(Math.sin(b) * Math.cos(e) + Math.cos(b) * Math.sin(e) * Math.sin(l));
}

function azimuth(H, phi, dec) {
	return Math.atan2(Math.sin(H), Math.cos(H) * Math.sin(phi) - Math.tan(dec) * Math.cos(phi));
}
function altitude(H, phi, dec) {
	return Math.asin(Math.sin(phi) * Math.sin(dec) + Math.cos(phi) * Math.cos(dec) * Math.cos(H));
}

function siderealTime(d, lw) {
	return RAD * (280.16 + 360.9856235 * d) - lw;
}

function astroRefraction(h) {
	// formula 16.4 of "Astronomical Algorithms" 2nd edition by Jean Meeus
	h = h < 0 ? 0 : h;
	return 0.0002967 / Math.tan(h + 0.00312536 / (h + 0.08901179));
}

// general sun calculations
function solarMeanAnomaly(d) {
	return RAD * (357.5291 + 0.98560028 * d);
}

function eclipticLongitude(M) {
	const perihelion = RAD * 102.9372;
	const centre = RAD * (1.9148 * Math.sin(M) + 0.02 * Math.sin(2 * M) + 0.0003 * Math.sin(3 * M));
	return M + centre + perihelion + PI;
}

function sunCoords(d) {
	const L = eclipticLongitude(solarMeanAnomaly(d));
	return {
		dec: declination(L, 0),
		ra: rightAscension(L, 0)
	};
}

function moonCoords(d) { // geocentric ecliptic coordinates of the moon
	var L = RAD * (218.316 + 13.176396 * d), // ecliptic longitude
	M = RAD * (134.963 + 13.064993 * d), // mean anomaly
	F = RAD * (93.272 + 13.229350 * d),  // mean distance

	l  = L + RAD * 6.289 * Math.sin(M), // longitude
	b  = RAD * 5.128 * Math.sin(F);     // latitude

	return {
		ra: rightAscension(l, b),
		dec: declination(l, b),
		dist: 385001 - 20905 * Math.cos(M) // distance to the moon in km
	};
};

function hoursLater(date, h) {
	return new Date(date.valueOf() + h * dayMs / 24);
}

export default {
	times: [
		// sun times configuration (angle, morning name, evening name)
		[-0.833, 'sunrise',       'sunset'      ],
		[  -0.3, 'sunriseEnd',    'sunsetStart' ],
		[    -6, 'dawn',          'dusk'        ],
		[   -12, 'nauticalDawn',  'nauticalDusk'],
		[   -18, 'nightEnd',      'night'       ],
		[     6, 'goldenHourEnd', 'goldenHour'  ]
	],
	addTime(angle, riseName, setName) {
		this.times.push([angle, riseName, setName]);
		return this;
	},
	getTimes(date, lat, lng, height = 0) {
		const lw = RAD * -lng,
			phi = RAD * lat,

			dh = observerAngle(height),

			d = toDays(date),
			n = julianCycle(d, lw),
			ds = approxTransit(0, lw, n),

			M = solarMeanAnomaly(ds),
			L = eclipticLongitude(M),
			dec = declination(L, 0),

			Jnoon = solarTransitJ(ds, M, L);
		
		let result = {
			solarNoon: fromJulian(Jnoon),
			nadir: fromJulian(Jnoon - 0.5)
		};

		for (let i = 0, len = this.times.length; i < len; i += 1) {
			let time = this.times[i];

			let Jset = getSetJ((Number(time[0]) + dh) * RAD, lw, phi, dec, n, M, L);
			let Jrise = Jnoon - (Jset - Jnoon);

			result[time[1]] = fromJulian(Jrise);
			result[time[2]] = fromJulian(Jset);
		}

		return result;
	},
	getMoonTimes(date, lat, lng, inUTC, t = new Date(date)) {
		if (inUTC)
			t.setUTCHours(0, 0, 0, 0);
		else
			t.setHours(0, 0, 0, 0);

		const hc = 0.133 * RAD;
		let ye, rise, set;
		let h0 = this.getMoonPosition(t, lat, lng).altitude - hc;

		// go in 2-hour chunks, each time seeing if a 3-point quadratic curve crosses zero (which means rise or set)
		for (let i = 1; i <= 24; i += 2) {
			let h1 = this.getMoonPosition(hoursLater(t, i), lat, lng).altitude - hc;
			let h2 = this.getMoonPosition(hoursLater(t, i + 1), lat, lng).altitude - hc;

			let a = (h0 + h2) / 2 - h1;
			let b = (h2 - h0) / 2;
			
			let x1, x2;
			let xe = -b / (2 * a);
			let d = b * b - 4 * a * h1;
			let roots = 0;
			ye = (a * xe + b) * xe + h1;

			if (d >= 0) {
				let dx = Math.sqrt(d) / (Math.abs(a) * 2);
				x1 = xe - dx;
				x2 = xe + dx;
				Math.abs(x1) <= 1 && roots++;
				Math.abs(x2) <= 1 && roots++;
				x1 < -1 && (x1 = x2);
			}

			if (roots === 1) {
				if (h0 < 0)
					rise = i + x1;
				else
					set = i + x1;
			} else if (roots === 2) {
				rise = i + (ye < 0 ? x2 : x1);
				set = i + (ye < 0 ? x1 : x2);
			}

			if (rise && set) break;

			h0 = h2;
		}

		var result = {};

		if (rise) result.rise = hoursLater(t, rise);
		if (set) result.set = hoursLater(t, set);

		if (!rise && !set) result[ye > 0 ? 'alwaysUp' : 'alwaysDown'] = true;

		return result;
	},
	getPosition(date, lat, lng) {
		const phi = RAD * lat;
		const d = toDays(date);
		const c  = sunCoords(d);
		const H  = siderealTime(d, RAD * -lng) - c.ra;
	
		return {
			azimuth: azimuth(H, phi, c.dec),
			altitude: altitude(H, phi, c.dec)
		};
	},
	getMoonPosition(date, lat, lng) {
		const lw  = RAD * -lng;
		const phi = RAD * lat;
		const d = toDays(date);

		const c = moonCoords(d);
		const H = siderealTime(d, lw) - c.ra;
		const h = altitude(H, phi, c.dec);

		return {
			azimuth: azimuth(H, phi, c.dec),
			altitude: h + astroRefraction(h), // Light bends through the atmosphere, dumbdumb.
			distance: c.dist,
			parallacticAngle: Math.atan2(Math.sin(H), Math.tan(phi) * Math.cos(c.dec) - Math.sin(c.dec) * Math.cos(H))
		};
	}
};
