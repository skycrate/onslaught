import Field from "./field.js";

export default class Interface {
	get required() {
		return [];
	}
	get defaults() {
		return {};
	}
	validate(options) {
		if (!this.required.every(name =>
			name.split('|')
				.map(key => options.hasOwnProperty(key))
				.reduce((a, b) => a || b))) {
					throw `Interface ${
						this.constructor.name
					} at requires ${
						name.replace('|', ' or ')
					} to be defined but only ${key} was passed.`;
				}
		return Object.fromEntries(Object.entries(this).map(([key, _value]) => {
			if (!options.hasOwnProperty(key))
				return [key, this.defaults.hasOwnProperty(key) ?
					this.defaults[key] :
						_value.default ? _value.default() : _value.prototype ? undefined : _value];
			
			const value = options[key];
			if (_value instanceof Field && !_value.validate(value))
				throw _value.error(this, key, value);
			
			let _type = _value.prototype ? _value : _value.constructor;
			if (!value instanceof _type)
				throw `Interface ${
					this.constructor.name
				} at "${key}" is of type ${_type}. Value of type ${value.constructor} was passed.`;
			
			return [key, _type.prototype instanceof Interface ? new _type().validate(value) : value];
		}));
	}
}
