import Field from "./field.js";
import {Vector} from "./maths.js";

export default class Range extends Field {
	constructor(a, b) {
		super(new Vector(a, b));
	}
	validate(value) {
		return this.data[0] <= value && value <= this.data[1];
	}
	error(interf, key, value) {
		return `Interface ${
			interf.constructor.name
		} at "${key}" can only have values in the range of ${
			this.data.join(' and ')
		} (inclusive). The value ${value} was passed.`;
	}
}
