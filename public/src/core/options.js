import Field from "./field.js";

export default class Options extends Field {
	constructor(...options) {
		super(options);
	}
	default() {
		return this.data[0];
	}
	validate(value) {
		return this.data.includes(value);
	}
	error(interf, key, value) {
		return `Interface ${interf.constructor.name} at "${key}" can only have values ${
			this.data.map((opt, index, all, last = all.length - 1) =>
					`${index === last ? 'or ' : '' }${opt}`).join(', ')
		}. The value ${value} was passed instead.`
	}
}
