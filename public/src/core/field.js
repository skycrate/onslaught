export default class Field {
	data;
	constructor(data) {
		if (!this.validate || !this.error)
			throw `Fields must have both "validate" and "error" functions.`;
		this.data = data;
	}
}
