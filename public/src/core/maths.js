const express = a => typeof a === "function" ? a() : a;
const uuid_part = (size = 3) => ("000" + (random_int(64592, 66654) | 0).toString(36)).slice(-size);

export const add = (a, b) => a + b;
export const sub = (a, b) => a - b;
export const mul = (a, b) => a && b && a * b;
export const div = (a, b) => a && (b || NaN) && a / b;
export const sqr = a => a && a * a;
export const cube = a => a && a * a * a;

export const scale = f => a => mul(a, f);

export const async_mul = (a, b) => a && b && express(a) * express(b);

export const cubic = (p1 = 0, p2 = 0, p3 = 1, p4 = 1, t = 0, _dist = 1 - t) =>
						async_mul(p1, () => cube(_dist)) +
						async_mul(mul(p2, t), () => mul(sqr(_dist), 3)) +
						async_mul(async_mul(p3, () => sqr(t)), () => mul(_dist, 3)) +
						async_mul(p4, () => cube(t));

export const timing_function = (p1 = 0, p2 = 0, p3 = 1, p4 = 1, origin = 0, target = 1, dist = target - origin) =>
						t => origin + mul(dist, cubic(p1, p2, p3, p4, t));

export const random = (a = 0, b = 1) => a + Math.random() * (b - a);
export const random_int = (a, b) => Math.round(random(a, b));
export const random_item = list => list[random_int(0, list.length - 1)];

export const uuid = (_parts = 3, _output = "") =>
					_parts ? uuid(_parts - 1, _output + uuid_part()) : _output;

export class Integer extends Number {
	static [Symbol.hasInstance](value) {
		return typeof value === "bigint" ||
			typeof value === "number" && Number.isInteger(value);
	}
}

export class UInteger extends Integer {
	static [Symbol.hasInstance](value) {
		return value >= 0 && super[Symbol.hasInstance](value);
	}
}

export class Float extends Number {
	static [Symbol.hasInstance](value) {
		return typeof value === "number";
	}
}

export class UFloat extends Float {
	static [Symbol.hasInstance](value) {
		return value >= 0.0 && super[Symbol.hasInstance](value);
	}
}

export const vector = (...coords) => new Vector(...coords);
export class Vector extends Array {
	get x() {
		return this[0] || 0;
	}
	get y() {
		return this[1] || 0;
	}
	get z() {
		return this[2] || 0;
	}
	constructor(...args) {
		super(...args);
	}
	concat(vector, operator = add) {
		return this.map((x, i) => operator(x, vector[i] || 0));
	}
	add(vector) {
		return this.concat(vector);
	}
	sub(vector) {
		return this.concat(vector, sub);
	}
	mul(vector) {
		return this.concat(vector, mul);
	}
	div(vector) {
		return this.concat(vector, div);
	}
	scale(factor) {
		return this.map(scale(factor));
	}
	abs() {
		return this.map(Math.abs);
	}
	mag() {
		return Math.sqrt(this.map(sqr).reduce(add));
	}
	dist(vector) {
		return this.sub(vector).mag();
	}
	equals(vector) {
		return this.length === vector.length && this.every((x, i) => x === vector[i]);
	}
}
