import Options from "./core/options.js"
import Interface from "./core/interface.js";
import {Vector} from "./core/maths.js";

import Mapstract from "./mapstract.js";

const API_KEY = 'b8FJ_9rCGkwXhBWlhl18u5ODWknOWJXaQ3ln_gSjKXI';

class ThemeOptions extends Interface {
	base = Object;
	dawn = {};
	day = {};
	dusk = {};
	night = {};
	space = {}; // Takes care of the poles of the earth and skybox of the night sky.
	get required() {
		return ['base'];
	}
}

class SourceOptions extends Interface {
	url = String;
	type = new Options(
		'vector',
		'satellite'
	)
	get required() {
		return ['type'];
	}
}

class MapOptions extends Interface {
	canvas = HTMLCanvasElement;
	source = SourceOptions;
	theme = ThemeOptions;
	max_tilt = new Range(0, 90); // degrees towards ground from perpendicular plane.
	range = new Vector(1, 20);
	min_height = 20; // from ground to camera in metres.
	type = new Options(
		'planar',
		'sphere',
	);
	target = new Vector(55.860916, -4.251433); // DEFAULT: Glasgow, UK
	get required() {
		return ['canvas', 'source', 'theme'];
	}
	get defaults() {
		return {
			max_tilt: 80
		};
	}
}

export default class MapView extends Mapstract {
	themes; // TODO: implement the switching of themes here.
	constructor(harp, options) {
		super(harp);
		options = new MapOptions().validate(options);

		let view = new harp.MapView({
			canvas: options.canvas,
			theme: {
				...options.theme.base,
				...options.theme.space,
			}, // we'll inject the base theme here for the now...
			target: new harp.GeoCoordinates(...options.target),
			...(options.type === 'planar' ? {
				projection: harp.mercatorProjection,
				zoomLevel: 13.5,
				tilt: 45,
				enableShadows: true,
				enableMixedLod: true,
			} : {
				projection: harp.sphereProjection,
				zoomLevel: 1,
				tilt: 0,
				enableShadows: false,
				enableMixedLod: false,
				// dynamicPixelRatio: devicePixelRatio / 2,
			}),
			fovCalculation: {
				type: 'dynamic', // is dynamic fov necessary? test performance.
				fov: 45,
			},
			minCameraHeight: options.min_height,
			disableFading: true,
			// Does this make things easier for bloom effects? Would love a bloom
			// effect in the globe view perhaps...

			tileCacheSize: 500,
			quadTreeSearchDistanceDown: 5,
			quadTreeSearchDistanceUp: 7,
			powerPreference: harp.MapViewPowerPreference.HighPerformance,
			resourceComputationType: harp.ResourceComputationType.NumberOfTiles,

			maxNumVisibleLabels: 50,
			numSecondChanceLabels: 0,
			labelDistanceScaleMin: 0.75,
			labelDistanceScaleMax: 1.5,
			maxDistanceRatioForTextLabels: 0.5,
			maxDistanceRatioForPoiLabels: 0.5,
			delayLabelsUntilMovementFinished: true,
		});
		view.beginAnimation();
		// This is a hack, we should be using zed/zui...
		options.canvas.map = this;

		view.addDataSource(options.source.type === 'vector' ?
			new harp.VectorTileDataSource({
				baseUrl: options.source.url || "https://vector.hereapi.com/v2/vectortiles/base/mc",
				authenticationCode: API_KEY
			}) :
			new harp.HereWebTileDataSource({
				apikey: API_KEY,
				tileBaseAddress: options.source.url || harp.HereTileProvider.TILE_AERIAL_SATELLITE
			})
		);
		let controller = new harp.MapControls(view);
		controller.minZoomLevel = options.range[0];
		controller.maxZoomLevel = options.range[1];
		controller.maxTiltAngle = options.max_tilt;

		console.log('****************************************');
		this.load(view, controller);
		console.log('****************************************');
	}
}
