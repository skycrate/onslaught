export default {
	sky: {
		type: "gradient",
		topColor: "#015ebb",
		bottomColor: "#dddcda",
		groundColor: "#87959a",
		monomialPower: 1
	},
	fog: {
		color: "#a8cde0",
		startRatio: 0.8
	},
}
