export default {
	sky: {
		type: "cubemap",
		positiveX: "/styles/sky-box/3.jpg",
		negativeX: "/styles/sky-box/1.jpg",
		positiveY: "/styles/sky-box/2.jpg",
		negativeY: "/styles/sky-box/6.jpg",
		positiveZ: "/styles/sky-box/5.jpg",
		negativeZ: "/styles/sky-box/4.jpg"
	},
	fog: {
		color: "#300A00",
		startRatio: 0.9
	},
	lights: [
		{
			type: "ambient",
			color: "#FFFFFF",
			name: "ambientLight",
			intensity: 0.9
		},
		{
			type: "directional",
			color: "#CCCBBB",
			name: "atmosphere",
			intensity: 0.8,
			direction: {
				x: 1,
				y: 5,
				z: 0.5
			}
		},
		{
			type: "directional",
			color: "#F4DB9C",
			name: "refraction",
			intensity: 0.8,
			direction: {
				x: -1,
				y: -3,
				z: 1
			}
		}
	],
	styles: {
		polar: [
			{
				description: "North pole",
				when: ["==", ["get", "kind"], "north_pole"],
				technique: "fill",
				renderOrder: 10,
				color: "#112f4a"
			},
			{
				description: "South pole",
				when: ["==", ["get", "kind"], "south_pole"],
				technique: "fill",
				renderOrder: 5,
				color: "#fcfdfd"
			}
		]
	}
}
