export default {
	sky: {
		type: "gradient",
		topColor: "#4d5b85",
		bottomColor: "#e3d196",
		groundColor: "#87959a",
		monomialPower: 1
	},
	fog: {
		color: "#e0a582",
		startRatio: 0.9
	},
}
