export default {
	sky: {
		type: "gradient",
		topColor: "#164e87",
		bottomColor: "#e6b191",
		groundColor: "#5d6466",
		monomialPower: 1
	},
	fog: {
		color: "#e68a7e",
		startRatio: 0.8
	}
}
