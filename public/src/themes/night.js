export default {
	sky: {
		type: "cubemap",
		positiveX: "/styles/sky-box/3.jpg",
		negativeX: "/styles/sky-box/1.jpg",
		positiveY: "/styles/sky-box/2.jpg",
		negativeY: "/styles/sky-box/6.jpg",
		positiveZ: "/styles/sky-box/5.jpg",
		negativeZ: "/styles/sky-box/4.jpg"
	},
	fog: {
		color: "#262829",
		startRatio: 0.8
	},
	lights: [
		{
			type: "ambient",
			color: "#C9CACC",
			name: "ambient", // guarde ca?
			intensity: 0.8
		}
	]
}
