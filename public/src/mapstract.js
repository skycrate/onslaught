import {Vector, timing_function} from "./core/maths.js";
import Interface from "./core/interface.js";
import Range from "./core/range.js";

const VOID = () => undefined;
const within_range = (value, start, end) => Math.max(start, Math.min(end, value));
const within_bounds = (value, start, end) => value > end ? start : value < start ? end : value;
const TIMING_FACTOR = 1000;

const Observer = callback =>
	new ResizeObserver(events => events.forEach(event =>
									callback(event.target, event.contentRect)));
const Resizer = Observer(element => element.map.resize());

class Animation {
	_step = VOID;
	constructor(stepping_function) {
		this._step = stepping_function;
	}
	step(timestamp = Date.now()) {
		return this._step.call(this, timestamp);
	}
}

class Tween extends Animation {
	start_time;
	duration;
	constructor(origin, target, duration = 250, delay = 0) {
		const start = Date.now() + delay;
		const cubic = timing_function(0, 1, 1, 1, origin, target);
		super(timestamp => cubic(within_range((timestamp - start) / duration, 0, 1)));
		this.start_time = start;
		this.duration = duration;
	}
}

class VectorTween extends Animation {
	start_time;
	duration;
	constructor(origin, target, duration = 250, delay = 0) {
		const start = Date.now() + delay;
		const cubicX = timing_function(0, 1, 1, 1, origin.x, target.x);
		const cubicY = timing_function(0, 1, 1, 1, origin.y, target.y);
		super(timestamp => {
			const time = within_range((timestamp - start) / duration, 0, 1);
			return new Vector(cubicX(time), cubicY(time))
		});
		this.start_time = start;
		this.duration = duration;
	}
}

class Pan extends VectorTween {
	// DURATION = Essentially, every one "unit" on the lat-long grid
	// takes one second to animate towards. Makes'er easy!
	constructor(origin, target, duration = Math.min(5000, origin.dist(target) * TIMING_FACTOR)) {
		super(origin, target, duration);
	}
}

class Zoom extends Tween {
	constructor(origin, target, duration = Math.abs(target - origin) * TIMING_FACTOR) {
		super(origin, target, duration);
	}
}

class Pitch extends Tween {
	constructor(origin, target, duration = TIMING_FACTOR) {
		super(origin, target, duration);
	}
}

class Yaw extends Tween {
	constructor(origin, target, duration = TIMING_FACTOR / 2) {
		super(origin, target, duration);
	}
}

class Orbit extends Animation {
	position;
	constructor(position) {
		super(() => {
			let position = this.position.add(
				new Vector(0.05, 0.05));
			return this.position = new Vector(
				within_bounds(position.x, -90, 90),
				within_bounds(position.y, -180, 180)
			);
		});
		this.position = position;
	}
}

class Spin extends Animation {
	azimuth = 0;
	constructor(azimuth) {
		super(() => {
			return this.azimuth += 0.1;
		});
	}
}

class FocusOptions extends Interface {
	position = Vector;
	azimuth = Number;
	zoom = new Range(1, 20);
	tilt = new Range(0, 90);
	get required() {
		return ['position'];
	}
}
export default class Mapstract {
	_harp; // harp...
	_view; // harp.MapView
	_controller; // harp.MapControls
	_animations = {
		position: null,
		zoom: null,
		tilt: null,
		azimuth: null
	};
	_render;
	get position() {
		const coords = this._view.target;
		return new Vector(coords.latitude, coords.longitude);
	}
	set position(vector) {
		return this._animations.position = new Pan(this.position, new Vector(...vector));
	}
	get zoom() {
		return this._view.zoomLevel;
	}
	set zoom(level) {
		return this._animations.zoom = new Zoom(this.zoom, within_range(level, this._controller.minZoomLevel, this._controller.maxZoomLevel));
		// return this._controller.setZoomLevel(
			// within_range(level, this._controller.minZoomLevel, this._controller.maxZoomLevel));
	}
	get tilt() {
		return this._view.tilt;
	}
	set tilt(angle) {
		return this._animations.tilt = new Pitch(this.tilt, within_range(angle, 0, this._controller.maxTiltAngle));
	}
	get azimuth() {
		return this._view.heading % 360;
	}
	set azimuth(angle) {
		angle = angle % 360;
		return this._animations.azimuth = new Yaw(this.azimuth, this.azimuth > angle + 180 ? angle + 360 : angle);
	}
	constructor(harp) {
		this._harp = harp;
	}
	orbit(options) {
		return this._animations.azimuth = new Spin(this.azimuth);
	}
	load(view, controller) {
		console.log(`SETTING VIEW for ${this.constructor.name}`);
		this.unload();
		this._view = view;
		this._controller = controller;
		this._render = (function(e) {
			return this.render();
		}).bind(this); // This does le trick!
		controller.enabled = true;
		view.addEventListener('render', this._render);
		Resizer.observe(view.canvas);
		return this;
	}
	unload(view = this._view, controller = this._controller) {
		console.debug(`UNLOADING VIEW for ${this.constructor.name}`);
		if (view) {
			controller.enabled = false;
			view.removeEventListener('render', this._render);
			Resizer.unobserve(view.canvas);
		}
		return this;
	}
	render(timestamp = Date.now()) {
		const anims = this._animations;
		const params = {
			target: new this._harp.GeoCoordinates(...(anims.position ? anims.position.step(timestamp) : this.position)),
			zoomLevel: anims.zoom ? anims.zoom.step(timestamp) : this.zoom,
			tilt: anims.tilt ? anims.tilt.step(timestamp) : this.tilt,
			heading: anims.azimuth ? anims.azimuth.step(timestamp) : this.azimuth,
		};
		requestAnimationFrame(ts => this._view.lookAt(params));
	}
	resize(width = window.innerWidth, height = window.innerHeight) {
		this._view.resize(width, height);
		return this;
	}
}
