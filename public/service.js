var cache_id = 'onslaught-app';
var files = [];

// OK, this is cool. We'll leverage this in the future.
self.addEventListener('install', e => e.waitUntil(
	caches.open(cache_id).then(cache => cache.addAll(files))
));
self.addEventListener('fetch', e => e.respondWith(
	caches.match(e.request).then(response => response || fetch(e.request))
));
