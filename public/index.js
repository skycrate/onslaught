import harp from "./harp.js";
import Globe from "./src/views/globe.js";
import City from "./src/views/city.js";
// import Interface from "./src/core/interface.js";
import { Vector } from "./src/core/maths.js";

Object.assign(HTMLElement.prototype, {
	reveal() {
		frame(ts => {
			this.style.left = 0;
			this.style.opacity = 1;
		});
		this.dispatchEvent(event('reveal'));
	},
	hide() {
		this.addEventListener('transitionend', function listener(e) {
			let opacity = this.style.opacity || 0;
			if (opacity == 0) // could be '0'
				this.style.left = '-100vw';
			this.removeEventListener('transitionend', listener, false);
		}, false);
		frame(ts => this.style.opacity = 0);
		this.dispatchEvent(event('hide'));
	}
});

console.log(`
 _______  _______  ___      _______  _______  _______ 
|   _   ||       ||   |    |   _   ||   _   ||       |
|  |_|  ||_     _||   |    |  |_|  ||  |_|  ||  _____|
|       |  |   |  |   |    |       ||       || |_____ 
|       |  |   |  |   |___ |       ||       ||_____  |
|   _   |  |   |  |       ||   _   ||   _   | _____| |
|__| |__|  |___|  |_______||__| |__||__| |__||_______|
   
`);
console.log(`(c) ${(new Date()).getFullYear()} SkyCrate Ltd.`);

const DO_NOTHING = () => {};
const id = id => document.getElementById(id);
const frame = callback => requestAnimationFrame(callback);
const delay = (delay, callback) => setTimeout(() => frame(callback), delay);
const event = (channel, data = {}, bubbles = false, cancelable = true) =>
	Object.assign(new Event(channel, {
		bubbles,
		cancelable
	}), data);

window.GLOBE = null;
window.CITY = null;

class View {
	dom;
	constructor(handler) {
		console.log(`CREATING VIEW FOR "${handler.id}"`);
		this.dom = id(handler.id);
		Object.assign(this, handler);
		this.init && this.init();
	}
	reveal(options) {
		this.dom.reveal();
		this.load && this.load(options);
	}
	hide() {
		this.dom.hide();
		this.unload && this.unload();
	}
}

window.ViewManager = {
	current: null,
	load(view, options) {
		this.current && this.current.hide();
		this.current = view;
		delay(500, () => this.current.reveal(options));
	},
};

window.VIEWS = Object.fromEntries(
	Object.entries({
		loading: {
			id: "loading_screen",
			load(options) {
				delay(options.delay || 3000, () =>
					ViewManager.load(options.view, options.options));
			}
		},
		install_prompt: {
			id: "install_prompt",
			install_btn: id("install_CTA"),
			init() {
				window.addEventListener('beforeinstallprompt', e => {
					console.log('DO WE EVEN GET HERE, BRO?');
					e.preventDefault();
					const deferredPrompt = e;

					this.install_btn.style.display = 'block';
					this.install_btn.addEventListener('click', e => {
						console.log(`WE CLICKED THE INSTALL BUTTON, DOG.`);
						this.install_btn.style.display = 'none';
						
						deferredPrompt.prompt();
						deferredPrompt.userChoice.then(choice => {
							if (choice.outcome === 'accepted') {
								console.log('USER ACCEPTED INSTALL. DISPLAY SOMETHING THAT SAYS DATS COOL');
								ViewManager.load(VIEWS.install_confirmed);
							} else {
								console.log('USER SAYS NO.');
								ViewManager.load(VIEWS.install_denied);
							}
							deferredPrompt = null;
						});
					}, false);
				});
			}
		},
		install_confirmed: {
			id: "install_confirmed",
		},
		install_denied: {
			id: "install_denied",
		},
		globe: {
			id: "globe_view",
			canvas: id("globe"),
			location_btn: id("use_current_location"),
			init() {
				let callback = e => {
					this.location_btn.hide();

					navigator.geolocation.getCurrentPosition(position => {
						GLOBE.position = new Vector(
							position.coords.latitude,
							position.coords.longitude
						);
						
						delay(3500, () => {
							GLOBE.zoom = 12;
							GLOBE.tilt = 55;

							ViewManager.load(VIEWS.city);
							VIEWS.city.hide();
							delay(3000, () => VIEWS.city.reveal());
						})
					}, () => {
						GLOBE.position = new Vector(55.860916, -4.251433);

						delay(100, () => {

							GLOBE.zoom = 12;
							GLOBE.tilt = 65;

							delay(3000, () => ViewManager.load(VIEWS.city));
						})
					});
				};
				this.location_btn.addEventListener('click', callback);
				// this.location_btn.addEventListener('touchend', callback);
				this.location_btn.hide();
			},
			load(options) {
				CITY = CITY || new City(harp, VIEWS.city.canvas);
				GLOBE.zoom = 4.5;
				GLOBE.tilt = 25;
				GLOBE.orbit();
				delay(3000, () => {
					// Reveal UI elements:
					this.location_btn.reveal();
				});
			},
			unload() {
				// do something with the map controls... (i.e. enable/disable)
			}
		},
		city: {
			id: "city_view",
			canvas: id("city"),
			load(options) {
				// position or whatever...
				CITY.position = GLOBE.position;
				CITY.zoom = 16.5;
				CITY.tilt = 50;
				CITY.orbit();
			},
			unload() {
				// do something with the map controls... (i.e. enable/disable)
			}
		},
	}).map(([key, handler]) => [key, new View(handler)]));

window.addEventListener('DOMContentLoaded', () => {
	navigator.serviceWorker && navigator.serviceWorker.register('/service.js');
	
	if (location.href.includes('.dev') || navigator.standalone || !!window.matchMedia('(display-mode: standalone)').matches) {
		console.log('Launching loading screen...');
		console.log('Setting up maps and UI...');

		GLOBE = GLOBE || new Globe(harp, VIEWS.globe.canvas);

		ViewManager.load(VIEWS.loading, {
			delay: 3500,
			view: VIEWS.globe,
			options: {
				// We should pull from local storage...
				// see what's up.
			}
		});
	} else {
		console.log('Setting up install screen');
		ViewManager.load(VIEWS.loading, {
			delay: 1280,
			view: VIEWS.install_prompt,
			options: {
				// a faire...
			}
		});
	}

	document.documentElement.requestFullscreen().catch(e => console.warn('Could not launch page into fullscreen. May require "click"'));
});

// Right, so what we worry about when we awake is:
	// our dom elements and their controls... searching a city, etc...
	// Get an atlas module working with H for the globe... need to pass for datasource and search...
	// controls for managing the views upon load, clicks, etc... (# url listening for some?)

/*const renderer = new THREE.WebGLRenderer({canvas});
const camera = new THREE.PerspectiveCamera(
	45, (({width, height}) =>
			width / height)(viewport()),
	1, 500
);
const scene = new THREE.Scene();
const material = new THREE.LineBasicMaterial({ color: 0x0000ff });
const geometry = new THREE.BufferGeometry().setFromPoints([
	[-10, 0, 0],
	[0, 10, 0],
	[10, 0, 0]
].map(pts => new THREE.Vector3(...pts)));

function init() {
	camera.position.set(0, 0, 100);
	camera.lookAt(0, 0, 0);
	
	scene.add(new THREE.Line(geometry, material));
	window.addEventListener('resize', resize, false);

	requestAnimationFrame(render);
};

function render(ts) {
	renderer.render(scene, camera);
	requestAnimationFrame(render);
};*/

// OKAY, we should maybe think about what WE wanna make. Eventually a custom element,
// but, for now, we are essentially making a controller. It works for both views, but
// with different parameters.
// The styles are to be passed for sunrise/sunset or for default...
// tilt range
// altitude range
// 
// const API_KEY = 'b8FJ_9rCGkwXhBWlhl18u5ODWknOWJXaQ3ln_gSjKXI';
// const coordinates = new harp.GeoCoordinates(55.860916, -4.251433);
// const targets = {
// 	distance: 200000,
// 	tilt: 12.5,
// 	azimuth: 0
// };
// const globe = new harp.MapView({
// 	canvas: document.getElementById("globe_scene"),
// 	theme: "/styles/berlin-globe.json",
// 	maxFps: 30,
// 	enableShadows: true
// });
// const city_map = new harp.MapView({
// 	canvas: document.getElementById("city_scene"),
// 	theme: "/styles/berlin-day.json", // switches to "/styles/berlin-dark.json" or "/styles/berlin-streets.json"
// 	maxFps: 40,
// 	enableShadows: true
// });
// const controls = new harp.MapControls(map);

// function init(map) {
// 	controls.maxTiltAngle = 80;
// 	// map.renderLabels = false;
// 	map.delayLabelsUntilMovementFinished = true;
// 	map.fog.enabled = false;

// 	map.addDataSource(new harp.VectorTileDataSource({
// 		baseUrl: "https://vector.hereapi.com/v2/vectortiles/base/mc",
// 		authenticationCode: API_KEY
// 	}));
// 	//map.addEventListener(harp.MapViewEventNames.Render, render);
// 	window.addEventListener('resize', resize);
// };

// function render() {
// 	requestAnimationFrame(ts => {
// 		map.lookAt(
// 			coordinates,
// 			options.distance,
// 			options.tilt,
// 			options.azimuth
// 		)
// 	});
// };

// function viewport(_el = window) {
// 	return {
// 		width: _el.innerWidth,
// 		height: _el.innerHeight
// 	}
// };

// function resize() {
// 	const args = Object.values(viewport());
// 	globe.resize(...args);
// 	city_map.resize(...args);
// };

// // INIT:
// init();
